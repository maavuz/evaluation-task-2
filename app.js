const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const connectedClients = [];

// Data file to keep the code DRY
const data = require('./data.json');

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

/**
 * Scince we dont have the actual data here,
 * use this function to generate random data and send it to connected clients
 *
 * @param {Array} arr Pass data array
 * @returns {Object} random data object from given array
 */
function getRandomData(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

/**
 * this function checks if the socket id exists in connected clients.
 *
 * @param {String} socketId socket id to find
 * @returns {String} Returns the index of the first element in the array where predicate is true, and -1 otherwise.
 */
function ClientExists(socketId) {
  // return connectedClients.some(e => e.socket_id === socketId);
  return connectedClients.findIndex(socket => socket.socket_id == socketId);
}

// You can give a Socket.io server arbitrary functions via io.use() that are run when a socket is created.
// we are using it to authenticate connection
io.use(function(socket, next) {
  // Get handshake data
  const handshakeData = socket.handshake.query;
  const userName = handshakeData.username;
  const userPassword = handshakeData.password;
  const client = handshakeData.clientId;

  // verify login credentials. this is only for demostration purposes. obviously we won't hard-code login credentials and verify these like this.
  // TODO: Use a JWT (https://www.npmjs.com/package/socketio-jwt) for authentication
  if (userName === 'demo' && userPassword === 'demo') {
    // if this client is not already stored.
    if (ClientExists(socket.id) == '-1') {
      // store client information
      connectedClients.push({ clientId: client, socket_id: socket.id });
    }

    // All good. lets proceed next.
    next();
  } else {
    // Auth failed. send error
    next(
      new Error(
        "{ 'code': 401, 'message': 'Authentication Error, please try again'}"
      )
    );
  }
});

// When a new socket connection is established
io.on('connection', function(socket) {
  // Send auth feedback
  socket.emit('message', 'Authentication successful, welcome');

  // Send Random Data to all connected socket clients every 5 seconds
  setInterval(function() {
    // get random data object
    const message = getRandomData(data);
    // get above object's clientId
    const client = message.clientId;
    // Loop through the connected sockets
    connectedClients.forEach(function(el) {
      // if conected clients' id match with data's client id
      if (el.clientId == client) {
        // Bingo! send
        io.to(el.socket_id).emit('message', message);
      }
    });
  }, 5000);

  // reomve client info on disconect
  socket.on('disconnect', function() {
    connectedClients.splice(
      connectedClients.findIndex(function(el) {
        return el.socket_id === socket.id;
      }),
      1
    );
  });
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
